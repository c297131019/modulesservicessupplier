plugins {
    java
    `java-gradle-plugin`
    kotlin("jvm") version("1.6.10")
}

java {
    sourceCompatibility = JavaVersion.VERSION_11
    targetCompatibility = JavaVersion.VERSION_11
}

repositories {
    google()
    mavenCentral()
    gradlePluginPortal()
}

dependencies {
    //添加Gradle相关的API，否则无法自定义Plugin和Task
    implementation(gradleApi())
    implementation(kotlin("stdlib-jdk8"))
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.5.1")
    implementation(gradleKotlinDsl())
//    implementation("com.android.tools.build:gradle:7.1.2")
    implementation ("org.javassist:javassist:3.28.0-GA")
}